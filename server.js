const express = require('express')
const http = require('http')
const bodyParser = require('body-parser')
const path = require('path')
const port = process.env.PORT || 8080
const app = express()
const server = http.createServer(app)

app.use(express.static(__dirname + '/build'))

// app.get('/*', (req, res) => res.sendFile(path.join(__dirname + '/index.html')))

app.get('/', (req, res) => res.send('Hello World!'))

server.listen(port)
