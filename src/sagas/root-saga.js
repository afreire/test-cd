import { spawn, call, all } from 'redux-saga/effects'

function makeRestartable(saga) {
  return function*() {
    while (true) {
      try {
        yield call(saga)
        throw new Error(`unexpected root saga termination - ${saga}`)
      } catch (err) {
        throw err
      }
    }
  }
}

const rootSagas = [].filter(s => s).map(makeRestartable)

// Combine sagas
export default function* rootSaga() {
  yield all(rootSagas.map(saga => spawn(saga)))
}
