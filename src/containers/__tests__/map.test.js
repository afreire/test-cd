import React from 'react'
import ReactDOM from 'react-dom'
import { createStore } from 'redux'
import Map from '../map.jsx'
import rootReducer from '../../root-reducer'

const initialState = {}
const store = createStore(rootReducer, initialState)

describe('Map Component', () => {
  it('should render connected map with default state', () => {
    const el = document.createElement('div')

    ReactDOM.render(<Map store={store} />, el)
    ReactDOM.unmountComponentAtNode(el)
  })
})
