export const handleJsonResponse = [
  res =>
    res.json().then(
      json => {
        if (res.ok && !json.error) return json.result || json
        const error = new Error(json.error === 1 ? json.message : json.error)
        error.name = 'Invalid API Call'
        throw error
      },
      err => {
        const error = new Error(err.message)
        error.name = 'Invalid JSON'
        throw error
      }
    ),
  err => {
    const error = new Error(
      err.message === 'Failed to fetch'
        ? 'Check your network connection.'
        : err.message
    )
    error.name = err.name
    throw error
  }
]

//  Reject promise if not resolved before given {ms} time
const timeOut = (ms, promise) =>
  new Promise((resolve, reject) => {
    setTimeout(() => reject(new Error('Event timeout')), ms)
    promise.then(resolve, reject)
  })

const caller = (endpoint, methodName = 'GET', formData = null) =>
  timeOut(
    5000,
    fetch(`${endpoint}`, {
      method: methodName,
      body: formData,
      mode: 'cors'
    })
  )

export default caller
