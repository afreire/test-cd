import caller from '../caller'

const apiEndpoint = '/vehicles'

// Test For API caller time out
describe('Testing fetch API caller', () => {
  beforeEach(() => fetch.resetMocks())

  it('should resolve within 5ms timeout', async () => {
    fetch.mockResponseOnce(
      () =>
        new Promise(resolve => setTimeout(() => resolve({ status: 200 }), 1000))
    )
    try {
      const response = await caller(apiEndpoint)
      expect(response.status).toEqual(200)
      expect(fetch.mock.calls[0][0]).toEqual('/vehicles')
    } catch (error) {
      throw error
    }
  })
})
