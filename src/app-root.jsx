import React from 'react'
import { Switch, Route } from 'react-router-dom'

const Map = () => <h1>Hello World</h1>

const routes = [
  {
    path: '/',
    component: Map,
    exact: true
  }
]

const AppRoot = () => (
  <Route
    render={({ location }) => (
      <Switch location={location}>
        {routes.map((route, index) => (
          <Route
            {...route}
            key={index.toString()}
            path={typeof route.path === 'function' ? route.path() : route.path}
          />
        ))}
      </Switch>
    )}
  />
)

export default AppRoot
